import React, { Component } from 'react';
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';

import AuthProvider from './utils/Auth';
import ErrorComponent from './components/shared/ErrorComponent';
import LoginComponent from './components/login/LoginComponent';
import TodosComponent from './components/todo/TodosComponent';

import './App.css';

class App extends Component {
  render() {
    return (
        <AuthProvider>
          <Router>
            <Switch>
              <Route path='/todos'>
                <TodosComponent />
              </Route>
              <Route path='/login'>
                <LoginComponent />
              </Route>
              <Route component={ErrorComponent} />
            </Switch>
          </Router>
        </AuthProvider>
    );
  }
}

export default App;
