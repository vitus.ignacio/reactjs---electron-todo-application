const DATASOURCE_TODOS = 'TODOS',
  TOKEN_KEY = 'todo-app-token',
  USERNAME_KEY = 'todo-app-username',
  token = 'random-auth-token';

if (localStorage.length === 0) {
  localStorage.setItem(DATASOURCE_TODOS, []);
}

export const AuthServices = {
  authenticate: (username, password, callback) => {
    if (username === 'tester' &&
        password === '123456') { // Generate random token
      localStorage.setItem(TOKEN_KEY, token);
      localStorage.setItem(USERNAME_KEY, username);
      callback(true, username, token);
    }
    else {
      localStorage.setItem(TOKEN_KEY, null);
      localStorage.setItem(USERNAME_KEY, null);
      callback(false, username, token);
    }
  },
  authorize: () => {
    const token = localStorage.getItem(TOKEN_KEY);
    if (!token) return {
      role: 'unauthorized',
      permissions: [],
    }
    return {
      role: 'user',
      permissions: ['r', 'w'],
    };
  },
  getToken: () => localStorage.getItem(TOKEN_KEY),
  getUsername: () => localStorage.getItem(USERNAME_KEY),
}

export const DataServices = {
  addTodo: (todo) => {
    let allTodos = localStorage.getItem(DATASOURCE_TODOS);
    if (allTodos && todo) {
      // TODO: Check if item exists
      allTodos.push(todo);
    }
  },
  getAllTodos: () => {
    let allTodos = localStorage.getItem(DATASOURCE_TODOS);
    return allTodos || [];
  },
  deleteTodo: (id) => {
    let allTodos = localStorage.getItem(DATASOURCE_TODOS);
    if (id) {
      let deletableTodo = allTodos.filter((todo) => todo.Id === id);
      if (deletableTodo) {
        let indexOfDeletableTodo = allTodos.indexOf(deletableTodo);
        if (indexOfDeletableTodo) {
          allTodos.splice(indexOfDeletableTodo, 1);
          return allTodos; // return new list
        }
      }
    }
    return null;
  },
  getTodo: (id) => {
    let allTodos = localStorage.getItem(DATASOURCE_TODOS);
    if (id) {
      return allTodos.filter((todo) => todo.Id === id);
    }
    return null;
  },
  updateTodos: (todos) => {
    localStorage.setItem(DATASOURCE_TODOS, JSON.stringify(todos));
  },
}