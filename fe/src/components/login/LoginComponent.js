import React from "react";
import ReactDOM from "react-dom";
import { Redirect } from "react-router-dom";
import {
  Button,
  Form,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon
} from "reactstrap";

import { AuthConsumer } from '../../utils/Auth';
import { AuthServices } from '../../services/DataServices';
import Inject from '../../utils/Injector';

import "./LoginComponent.css";

class LoginComponent extends React.Component {

  /* Constructor, constants and static methods */
  constructor(props) {
    super(props);

    // Add DAL
    Inject(AuthServices, this)

    this.loadingIndicator = React.createRef();

    this.state = {
      email: undefined,
      password: undefined,

      redirectToRenderer: false,

      formValidationStatus: true,
      formValidationErrorMessage: null,
      formEmailValidationStatus: true,
      formEmailValidationErrorMessage: null,
      formPasswordValidationStatus: true,
      formPasswordValidationErrorMessage: null,
    };
  }

  /* React event lifecycle */
  componentDidMount() {
    this.loginFormEl = ReactDOM.findDOMNode(this.loginForm);
    this.emailFieldEl = ReactDOM.findDOMNode(this.emailField);
    this.passwordFieldEl = ReactDOM.findDOMNode(this.passwordField);

    setTimeout(() => {
      this.loadingIndicator.current.classList.toggle("fade");
    }, 2000);
  }

  /* Other internal methods */
  validate(email, password) {
    let isValid = this.loginFormEl.checkValidity();
    if (isValid) {
      this.authenticate(email, password, (status, username, token) => {
        if (status) {
          this.props.updateToken(username, token);
          this.setState({
            formValidationStatus: true,
            redirectToRenderer: true,
          });
        } else {
          this.setState({
            formValidationStatus: false,
            formEmailValidationStatus: true,
            formPasswordValidationStatus: true,
            formValidationErrorMessage: 'Wrong email or password.',
          });
        }
      });
    } else {
      this.setState({
        formValidationStatus: false,
        formValidationErrorMessage: 'Some fields have errors, please check below',
      });
      for (let i = 0; i < this.loginFormEl.length; i++) {
        let element = this.loginFormEl[i];
        switch(element.name) {
          case 'email':
            this.setState({
              formEmailValidationStatus: element.validity.valid,
              formEmailValidationErrorMessage: !element.validity.valid ? element.validationMessage : null,
            })
            break;
          case 'password':
            this.setState({
              formPasswordValidationStatus: element.validity.valid,
              formPasswordValidationErrorMessage: !element.validity.valid ? element.validationMessage : null,
            })
            break;
          default:
            break;
        }
      }
    }
  }

  /* React element events handlers */
  handleSubmit(event) {
    event.preventDefault();
    const { email, password } = this.state;
    this.validate(email, password);
  }

  onEmailChanged(event) {
    this.setState({
      email: event.target.value
    });
  }

  onPasswordChanged(event) {
    this.setState({
      password: event.target.value
    });
  }

  /* Render component */
  render() {
    const { 
      formValidationStatus, 
      formValidationErrorMessage,
      formEmailValidationStatus,
      formEmailValidationErrorMessage,
      formPasswordValidationStatus,
      formPasswordValidationErrorMessage,
      redirectToRenderer, 
    } = this.state;

    if (redirectToRenderer) {
      return <Redirect to="/todos" />
    }

    return (
      <div className={`login-wrapper`}>
        <div ref={this.loadingIndicator} className={`loading`}>
          Loading&#8230;
        </div>
        <Form
          ref={(form) => this.loginForm = form}
          className={`login-form`} noValidate
          onSubmit={event => this.handleSubmit(event)}
        >
          <br />
          { formValidationStatus === false ? 
          <div className={`login-form__validation-summary`}>
            <p className={`validation-error`}>{formValidationErrorMessage || ''}</p>
          </div> : ''}
          <FormGroup>
            <InputGroup className={`login-form__input-group`}>
              <InputGroupAddon addonType={`prepend`}>@Email Address</InputGroupAddon>
              <Input ref={(element) => this.emailField = element} name={`email`} onChange={event => this.onEmailChanged(event)} required />
            </InputGroup>
            {!formEmailValidationStatus ?
            <p className={`validation-error`}>{formEmailValidationErrorMessage}</p> : ''}
          </FormGroup>
          <FormGroup>
            <InputGroup className={`login-form__input-group`}>
              <InputGroupAddon addonType={`prepend`}>@Password</InputGroupAddon>
              <Input
                ref={(element) => this.passwordField = element}
                name={`password`}
                type={`password`}
                onChange={event => this.onPasswordChanged(event)}
                required
              />
            </InputGroup>
            {!formPasswordValidationStatus ?
            <p className={`validation-error`}>{formPasswordValidationErrorMessage}</p> : ''}
          </FormGroup>
          <Button className={`login-form__submit-button`}>Submit</Button>
        </Form>
      </div>
    );
  }
}

/* Connect LoginComponent with React Context */
const ConnectedLoginComponent = props => (
  <AuthConsumer>
      {({ token, updateToken }) => (
      <LoginComponent
          {...props}
          token={token}
          updateToken={updateToken}
      />
      )}
  </AuthConsumer>
)

export default ConnectedLoginComponent;
