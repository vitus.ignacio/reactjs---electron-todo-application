import React from 'react';
import {
  Card,
  CardText,
  CardBody,
  CardTitle,
  CardSubtitle,
} from 'reactstrap';

import { AuthConsumer } from '../../utils/Auth';
import { Constants } from '../../utils/Constants';
import Inject from '../../utils/Injector';
import { DataServices } from '../../services/DataServices';

import './TodoCardComponent.css';

class TodoCardComponent extends React.Component {

  /* Constructor and static methods */
  constructor(props) {
    super(props);

    Inject(DataServices, this);

    this.state = {...props}
  }

  /* React events cycle */
  componentDidMount() { }

  /* React events handlers */
  createNewTask(event, targetType) {
    let todo = prompt('What is new task name?');
    if (!todo) return;
    if (todo && todo.length === 0) return;
    let { todos, username } = this.props;
    switch(targetType) {
      case Constants.TODO_TYPE_TRIVIAL:
        todos.trivial.push({
          name: todo,
          createdOn: '06/03/2019',
          createdBy: username,
          isDone: false,
        });
        break;
      case Constants.TODO_TYPE_PRIORITY:
        todos.priority.push({
          name: todo,
          createdOn: '06/03/2019',
          createdBy: username,
          isDone: false,
        });
        break;
      case Constants.TODO_TYPE_URGENT:
        todos.urgent.push({
          name: todo,
          createdOn: '06/03/2019',
          createdBy: username,
          isDone: false,
        });
        break;
      default:
        break;
    }
    this.props.updateTodosList(todos);
  }

  render() {
    const {
      name,
      createdOn,
      createdBy
    } = this.state;
    const { isTodo, targetType } = this.props;

    return (
      <div>
        {isTodo ? 
        <Card className={`todo-card`}>
          <CardBody>
            <CardTitle>{name || Constants.TODO_UNNAMED}</CardTitle>
            <CardSubtitle>{createdOn || Constants.TODO_NO_CREATED_ON} by {createdBy || Constants.TODO_NO_CREATED_BY}</CardSubtitle>
            <a href={`#`}>Mark as done</a><br /><a href={`#`}>Delete</a>
          </CardBody>
        </Card> :
        <Card className={`todo-card`}>
          <CardBody>
            <CardText><a href={`#`} onClick={ (event) => this.createNewTask(event, targetType) }>Create a new todo</a></CardText>
          </CardBody>
        </Card>
        }
      </div>
    )
  }

}

/* Connect TodoCardComponent with React Context */
const ConnectedTodoCardComponent = props => (
  <AuthConsumer>
      {({ todos, username, updateTodosList }) => (
      <TodoCardComponent
          {...props}
          todos={todos}
          username={username}
          updateTodosList={updateTodosList}
      />
      )}
  </AuthConsumer>
)

export default ConnectedTodoCardComponent;
