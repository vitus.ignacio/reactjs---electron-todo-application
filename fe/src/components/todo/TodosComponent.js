import React from 'react';
import { Redirect } from 'react-router-dom';
import {
  Container,
  Row,
  Col,
} from 'reactstrap';

import { AuthConsumer } from '../../utils/Auth';
import { Constants } from '../../utils/Constants';
import TodoCardComponent from './TodoCardComponent';

import './TodosComponent.css';

class TodosComponent extends React.Component {

  /* Constructor and static methods */
  constructor(props) {
    super(props);

    this.state = {...props}
  }

  /* React events cycle */

  /* React events handlers */

  render() {
    const { token, todos } = this.state;

    if (!token) {
      return <Redirect to='/login' />
    }

    return (
      <div className={`tasks-wrapper`}>
        <Container style={{
          paddingTop: '60px'
        }}>
          <Row>
            <Col sx={6} sm={4}>
              <div className={`task-list__header`}>
                <h2>Trivial Tasks</h2>
              </div>
              {
                todos.trivial.map((todo, index) => <TodoCardComponent {...todo} key={index} isTodo={true} />)
              }
              <TodoCardComponent targetType={Constants.TODO_TYPE_TRIVIAL} isTodo={false} />
            </Col>
            <Col sx={6} sm={4}>
              <div className={`task-list__header priority`}>
                <h2>Priority Tasks</h2>
              </div>
              {
                todos.priority.map((todo, index) => <TodoCardComponent {...todo} key={index} isTodo={true} />)
              }
              <TodoCardComponent targetType={Constants.TODO_TYPE_PRIORITY} isTodo={false} />
            </Col>
            <Col sm={4}>
              <div className={`task-list__header urgent`}>
                <h2>Urgent Tasks</h2>
              </div>
              {
                todos.urgent.map((todo, index) => <TodoCardComponent {...todo} key={index} isTodo={true} />)
              }
              <TodoCardComponent targetType={Constants.TODO_TYPE_URGENT} isTodo={false} />
            </Col>
          </Row>
        </Container>
      </div>
    )
  }

}

/* Connect TodosComponent with React Context */
const ConnectedTodosComponent = props => (
  <AuthConsumer>
      {({ token, todos }) => (
      <TodosComponent
          {...props}
          token={token}
          todos={todos}
      />
      )}
  </AuthConsumer>
)

export default ConnectedTodosComponent;
