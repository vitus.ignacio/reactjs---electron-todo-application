import React from 'react';

import { Container, Row, Col } from 'reactstrap';

import './ErrorComponent.css';

export default class ErrorComponent extends React.Component {

  render() {
    return (
      <Container className={`wrapper`}>
        <Row>
          <Col>
            <div className={`error`}>
              <div className={`error__wrapper`}>
                <h1 className={`error__wrapper__code`}>{`404`}</h1>
                <p className={`error__wrapper__message`}>{`Your resources cannot be found`}</p>
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    )
  }

}
