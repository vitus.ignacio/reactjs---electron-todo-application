import React from 'react';

import Inject from './Injector';
import { AuthServices, DataServices } from '../services/DataServices';

const AuthContext = React.createContext();

export const AuthConsumer = AuthContext.Consumer;

export default class AuthProvider extends React.Component {
  
  state = {
    token: null,
    todos: {
      trivial: [],
      priority: [],
      urgent: []
    },
    username: null,
    updateToken: (username, token) => this.updateToken(username, token),
    updateTodosList: (todos, type) => this.updateTodosList(todos, type),
  }

  constructor() {
    super();

    Inject(AuthServices, this);
    Inject(DataServices, this);
    
    this.state.token = this.getToken();
    this.state.username = this.getUsername();
  }

  componentWillMount() {
    const todos = this.getAllTodos() ? JSON.parse(this.getAllTodos()) : [];
    if (todos) {
      this.setState({
        todos: todos
      });
    }
  }

  updateToken(username, token) {
    this.setState({
      username: username,
      token: token
    });
  }

  updateTodosList(todos, type) {
    this.setState({
      todos: todos
    });
    this.updateTodos(todos);
  }

  render () {
    return (
       <AuthContext.Provider value={this.state}>
        {this.props.children}
      </AuthContext.Provider>
    )
  }
}
