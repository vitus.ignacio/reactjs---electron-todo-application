export default function Inject(fromKlass, toKlass) {
  for (let method in fromKlass) {
      if (typeof fromKlass[method] === 'function') {
          toKlass[method] = fromKlass[method];
      }
  }

  return toKlass;
}
