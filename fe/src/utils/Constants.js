export const Constants = {
  TODO_UNNAMED: 'Unnamed',
  TODO_NO_DUE_ON: 'xx/xx/xxxx',
  TODO_NO_CREATED_ON: 'xx/xx/xxxx',
  TODO_NO_CREATED_BY: 'Unauthored',
  TODO_NO_DESCRIPTION: 'Undescribed',
  TODO_TYPE_TRIVIAL: 'trivial',
  TODO_TYPE_PRIORITY: 'priority',
  TODO_TYPE_URGENT: 'urgent'
}
